Aplicación responsive multidioma para visualizar y comprar teléfonos móviles.
React 18, Material UI v5, axios, react-router

Componentes:
- App:
    - Componente Enrutador.
    - Gestor de estilos por temas.
    - Botón para hacer scroll arriba.
    - Footer.

- ApplicationBar:
    - Título: clickable para ir a la home.
    - Breadcrumbs.
    - Switch para modo oscuro.
    - Botón carrito.
    - Selector de idioma (inglés, español).

- MobileList:
    - Visualizador de los dispositivos móviles traídos del backend.
    - Ordenados por páginas.
    - Filtrar dinámicamente por marca, modelo o ambos.
    - Filtrar por precio (menor a mayor, mayor a menor).
    - Cambiar número de productos que aparecen por página.

- MobileDescription:
    - Visualización de la descripción del dispositivo móvil elegido.
    - Selección de colores y almacenamientos.
    - Acciones para agregar artículo al carrito o volver a lista de artículos.

- CartView:
    - Vista del carrito.
    - Resumen de los artículos añadidos al carrito.
    - Resumen de la compra.

- Footer:
    - Footer con posibles links de una aplicación real de compra de móviles

- Calls:
    - Gestor de llamadas con intentos

- LocalStorageCache:
    - Funciones para la gestión de la persistencia de datos.
    - Uso de Local storage para que la información se comparta entre pestañas.


Tests:
- npm test: lanzar tests de renderización de algunos componentes y comprobación del buen comportamiento de algunos de estos como el botón de cambio de idioma o el link a la home del título