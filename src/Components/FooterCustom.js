import { Facebook, Instagram, Pinterest, Twitter, YouTube } from '@mui/icons-material';
import { Typography, List, Box, ListItem, Link } from '@mui/material';
import textsJSON from "../Texts/texts.json";

const texts = textsJSON.FooterCustom; //Texts for multilanguage

/*
  Footer component
  It has possible links of a real mobile shop online application
  with icons of its possible social networks
*/
export default function FooterCustom({ language, theme }) {
  return (
    <footer className='footer-div'>
      <Box sx={{
        width: "100%",
        height: 200,
        backgroundColor: theme === "Dark Theme" ? '#3e3b40c9' : '#4383cc47',
      }}
        className="div-row-footer"
      >
        <div className='div-row'>
          <div>
            <Typography className="footer-title">{texts.Help[language]}</Typography>
            <List>
              <ListItem>
                <Link underline="hover"
                  sx={{ display: 'flex', alignItems: 'center' }}
                  color="inherit"
                >
                  {texts.Contact[language]}
                </Link>
              </ListItem>
              <ListItem>
                <Link underline="hover"
                  sx={{ display: 'flex', alignItems: 'center' }}
                  color="inherit"
                >
                  {texts.Support[language]}
                </Link>
              </ListItem>
              <ListItem>
                <Link underline="hover"
                  sx={{ display: 'flex', alignItems: 'center' }}
                  color="inherit"
                >
                  {texts.Privacy[language]}
                </Link>
              </ListItem>
              <ListItem>
                <Link underline="hover"
                  sx={{ display: 'flex', alignItems: 'center' }}
                  color="inherit"
                >
                  {texts.LegalAdvice[language]}
                </Link>
              </ListItem>
            </List>
          </div>

          <div>
            <Typography className="footer-title">{texts.Account[language]}</Typography>
            <List>
              <ListItem>
                <Link underline="hover"
                  sx={{ display: 'flex', alignItems: 'center' }}
                  color="inherit"
                >
                  {texts.Login[language]}
                </Link>
              </ListItem>
              <ListItem>
                <Link underline="hover"
                  sx={{ display: 'flex', alignItems: 'center' }}
                  color="inherit"
                >
                  {texts.Register[language]}
                </Link>
              </ListItem>
              <ListItem>
                <Link underline="hover"
                  sx={{ display: 'flex', alignItems: 'center' }}
                  color="inherit"
                >
                  {texts.Conditions[language]}
                </Link>
              </ListItem>
            </List>
          </div>
          <div>
            <Typography className="footer-title">{texts.Company[language]}</Typography>
            <List>
              <ListItem>
                <Link underline="hover"
                  sx={{ display: 'flex', alignItems: 'center' }}
                  color="inherit"
                >
                  {texts.WhoWeAre[language]}
                </Link>
              </ListItem>
              <ListItem>
                <Link underline="hover"
                  sx={{ display: 'flex', alignItems: 'center' }}
                  color="inherit"
                >
                  {texts.WorkWithUs[language]}
                </Link>
              </ListItem>
            </List>
          </div>
          <div className="div-social">
            <Facebook className="icons-paper" color="secondary" />
            <Instagram className="icons-paper" color="secondary" />
            <Twitter className="icons-paper" color="secondary" />
            <YouTube className="icons-paper" color="secondary" />
            <Pinterest className="icons-paper" color="secondary" />
          </div>
        </div>
      </Box>
    </footer>
  );
}