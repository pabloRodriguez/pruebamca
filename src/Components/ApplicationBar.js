import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { AppBar, IconButton, Button, Toolbar, Typography, Badge, Breadcrumbs, Link, Switch, Tooltip } from '@mui/material/';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import HomeIcon from '@mui/icons-material/Home';
import DescriptionIcon from '@mui/icons-material/Description';
import textsJSON from "../Texts/texts.json";
import { removeHtmlStorage } from '../Utils/LocalStorageCache';
import { Sell } from '@mui/icons-material';

const texts = textsJSON.ApplicationBar; //Texts for multilanguage

/*
  Application toolbar
  - Toolbar
  - Link title (click to go home)
  - Breadcrumbs with Links (Home -> Description)
  - Theme switch
  - Cart icon with items count
  - Button to change language (English - Spanish)
*/
export default function ApplicationBar({ language, setLanguage, products, theme, setTheme }) {
  const [themeChecked, setThemeChecked] = useState(false); //Theme switch controller state (false -> Light Theme, true -> Dark Theme)
  const navigate = useNavigate(); //Navigate through routes with react router

  //Switch theme handler
  const handleSwitchChange = (event) => {
    setThemeChecked(event.target.checked);
    if (event.target.checked) {
      setTheme('Dark Theme');
    } else {
      setTheme('Light Theme');
    }
  };

  //Change language handler (EN - ES)
  const handleLanguage = () => {
    if (language === 'EN') { setLanguage('ES'); }
    else { setLanguage('EN'); }
  }

  return (
    <AppBar role="heading" position="static">
      <Toolbar className='toolbar'>
        <div>
          <Typography onClick={() => { navigate('/'); removeHtmlStorage('mobile_info'); removeHtmlStorage('mobile_color'); removeHtmlStorage('mobile_storage'); }} className='typography-bar-title' component="span">
            The Mobile House
          </Typography>
          <Breadcrumbs aria-label="breadcrumb">
            <Link
              underline="hover"
              sx={{ display: 'flex', alignItems: 'center' }}
              color="inherit"
              href="/"
            >
              <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
              {texts.Breadcrumbs.Home[language]}
            </Link>
            {window.location.pathname === '/description' ?
              <Link
                underline="hover"
                sx={{ display: 'flex', alignItems: 'center' }}
                color="inherit"
                disabled
              >
                <DescriptionIcon sx={{ mr: 0.5 }} fontSize="inherit" />
                {texts.Breadcrumbs.Description[language]}
              </Link> : null}
              {window.location.pathname === '/cart' ?
              <Link
                underline="hover"
                sx={{ display: 'flex', alignItems: 'center' }}
                color="inherit"
                disabled
              >
                <Sell sx={{ mr: 0.5 }} fontSize="inherit" />
                {texts.Breadcrumbs.Cart[language]}
              </Link> : null}
          </Breadcrumbs>
        </div>
        <div className='div-flex' />
        <Switch role="switch" checked={themeChecked} onChange={handleSwitchChange} />
        <Typography className='typography-theme'>{theme === "Dark Theme" ? texts.Theme.Dark[language] : texts.Theme.Light[language]}</Typography>
        <Badge color="secondary" badgeContent={products.length}>
          <Tooltip aria-label={texts.Cart[language]} title={texts.Cart[language]} arrow>
            <IconButton onClick={() => { navigate('/cart'); removeHtmlStorage('mobile_info'); removeHtmlStorage('mobile_color'); removeHtmlStorage('mobile_storage'); }}>
              <ShoppingCartIcon aria-label='shop-button' fontSize='medium' />
            </IconButton>
          </Tooltip>
        </Badge>
        <Button color="secondary" className='button-language' variant="contained" onClick={handleLanguage}>{language}</Button>
      </Toolbar>
    </AppBar>
  );
}