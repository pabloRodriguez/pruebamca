import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, CircularProgress, List, ListItem, Paper, TextField, Pagination, Typography, Select, MenuItem } from "@mui/material";
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import StoreIcon from '@mui/icons-material/Store';
import { setHtmlStorage, statusHtmlStorage } from '../Utils/LocalStorageCache.js';
import { AxiosGet } from '../Utils/Calls.js';
import textsJSON from "../Texts/texts.json";

const texts = textsJSON.MobileList; //Texts for multilanguage

export const mobileDataUrl = 'https://front-test-api.herokuapp.com'; //Backend endpoint
export const mobileDataPathGet = '/api/product/'; //Backend get endpoint for mobile list and mobile description

/*
  Mobile list view
  - Search filters by brand and model
  - Filter by price (low to high, high to low)
  - Set pagination size number of products
  - List mobile products with its brand, model and price order by pages and filters
  - Show details button to go to description mobile view
  - Pagination controller
*/
export default function MobileList({ language, setMobileId, theme }) {
  const [mobileData, setMobileData] = useState(statusHtmlStorage('mobile_data', {})); //Mobile list information from backend
  const [textFieldDisabled, setTextFieldDisabled] = useState(true); //Disable textfield for searchig when there is no mobile information
  const [textFieldValue, setTextFieldValue] = useState(""); //Textfield search value input
  const [textFieldSelect, setTextFieldSelect] = useState("all"); //Filter select (All, Brand, Model)
  const [order, setOrder] = React.useState(0); //Order filter (0 no order, 1 price low to high, 2 price high to low)
  const [page, setPage] = React.useState(1); //Pagination state of mobiles
  const [paginationSize, setPaginationSize] = React.useState(20); //Pagination filter size
  const navigate = useNavigate(); //Navigate through routes with react router

  //Get mobile data from backend if it is not in Local Storage
  useEffect(() => {
    async function getMobileData() {
      //Check if mobiles data is in Local Storage and get it
      if (statusHtmlStorage('mobile_data')) {
        setMobileData(JSON.parse(localStorage.getItem('mobile_data')));
      //If it is not in Local Storage, get of backend and store it in Local Storage
      } else {
        const data = await AxiosGet(`${mobileDataUrl}${mobileDataPathGet}`);
        setHtmlStorage('mobile_data', JSON.stringify(data.data), 3600);
        setMobileData(data.data);
      }
      //Enable textfield to search when data is rendered
      setTextFieldDisabled(false);
    }
    getMobileData();
  }, []);

  //Search textfield handler
  const onChangeTextField = (value) => {
    setTextFieldValue(value);
  };

  //Pagination change handler
  const handlePaginationChange = (event, value) => {
    setPage(value);
  };

  //Return mobile list data taking into account all filters
  const renderMobileList = () => {
    let data = [];
    //1. Search filter
    if (textFieldValue) {
      const regExTextFieldValue = new RegExp(`${textFieldValue.toLowerCase().trim()}`);
      if (textFieldSelect === "all") {
        for (let i = 0; i < mobileData.length; i++) {
          if (mobileData[i].brand.toLowerCase().search(regExTextFieldValue, "g") !== -1 || mobileData[i].model.toLowerCase().search(regExTextFieldValue, "g") !== -1) {
            data.push(mobileData[i]);
          }
        }
      } else if (textFieldSelect === "brand") {
        for (let i = 0; i < mobileData.length; i++) {
          if (mobileData[i].brand.toLowerCase().search(regExTextFieldValue, "g") !== -1) {
            data.push(mobileData[i]);
          }
        }
      } else if (textFieldSelect === "model") {
        for (let i = 0; i < mobileData.length; i++) {
          if (mobileData[i].model.toLowerCase().search(regExTextFieldValue, "g") !== -1) {
            data.push(mobileData[i]);
          }
        }
      }
    } else {
      data = mobileData;
    }

    //2. Filter by price
    let dataWithoutPrice = []
    if (order === 1 || order === 2) {
      dataWithoutPrice = data.filter(item => item.price === "");
      data = data.filter(item => item.price !== "");
    }
    
    if (order === 1) {
      data.sort(function (a, b) {
        if (parseInt(a.price) < parseInt(b.price)) return -1;
        if (parseInt(a.price) > parseInt(b.price)) return 1;
        return 0;
      });
    } else if (order === 2) {
      data.sort(function (a, b) {
        if (parseInt(a.price) > parseInt(b.price)) return -1;
        if (parseInt(a.price) < parseInt(b.price)) return 1;
        return 0;
      });
    }

    if (order === 1 || order === 2) {
      for (let i = 0; i < dataWithoutPrice.length; i++) {
        data.push(dataWithoutPrice[i]);
      }
    }

    //3. Pagination
    let data_page = [];
    for (let i = (page - 1) * paginationSize; i < page * paginationSize; i++) {
      if (data[i]) {
        data_page.push(data[i]);
      } else {
        break;
      }
    }

    return (
      <div className='div-column' style={theme === "Dark Theme" ? { backgroundColor: "#1b1b1b" } : { backgroundColor: "#f9f9f9" }}>
        <div className="div-row-title">
          <Typography style={theme === "Dark Theme" ? { color: "white" } : {}} className='typography-mobileList-title'>{texts.Title[language]}</Typography>
          <div className="div-flex" />
          <Typography style={theme === "Dark Theme" ? { color: "white" } : {}} className='typography-mobileList-filter'>{texts.Filter[language]}</Typography>
          <TextField
            placeholder={texts.Search[language]}
            className='textfield-filter'
            aria-label='textfield-search'
            color="secondary"
            disabled={textFieldDisabled}
            value={textFieldValue}
            onChange={e => { onChangeTextField(e.target.value) }}
          />
          <Select
            labelId="mobile-select"
            color="secondary"
            id="filter-select"
            value={textFieldSelect}
            label="Filter"
            onChange={(event) => setTextFieldSelect(event.target.value)}
          >
            <MenuItem value={"all"}>{texts.Select.All[language]}</MenuItem>
            <MenuItem value={"brand"}>{texts.Select.Brand[language]}</MenuItem>
            <MenuItem value={"model"}>{texts.Select.Model[language]}</MenuItem>
          </Select>
          <Typography style={theme === "Dark Theme" ? { color: "white" } : {}} className='typography-mobileList-filter'>{texts.Order.Title[language]}</Typography>
          <Select
            labelId="order-select"
            color="secondary"
            id="select-order"
            value={order}
            label="Order"
            onChange={(event) => setOrder(event.target.value)}
          >
            <MenuItem value={0}>{texts.Order.NoOrder[language]}</MenuItem>
            <MenuItem value={1}>{texts.Order.PriceToHigh[language]}</MenuItem>
            <MenuItem value={2}>{texts.Order.PriceToLow[language]}</MenuItem>
          </Select>
          <Typography style={theme === "Dark Theme" ? { color: "white" } : {}} className='typography-mobileList-filter'>{texts.Pagination[language]}</Typography>
          <Select
            labelId="pagination-select"
            color="secondary"
            id="select-pagination"
            value={paginationSize}
            label="Pagination"
            onChange={(event) => setPaginationSize(event.target.value)}
          >
            <MenuItem value={15}>15</MenuItem>
            <MenuItem value={20}>20</MenuItem>
            <MenuItem value={25}>25</MenuItem>
          </Select>
        </div>
        {Object.keys(mobileData).length === 0 ?
          <CircularProgress style={{ width: "100px", height: "100px" }} className="circular-progress" /> :
          <div className="div-grid">
            {data_page.map(mobile => {
              return (
                <Paper className="mobile-papers" elevation={3} key={mobile.id}>
                  <div className="div-row">
                    <img src={mobile.imgUrl} alt={mobile.model} />
                    <div className="div-center-list">
                      <List>
                        {mobile.brand ?
                          <ListItem>
                            <StoreIcon color="secondary" className='icons-paper' />
                            {mobile.brand}
                          </ListItem>
                          : ""}
                        {mobile.model ?
                          <ListItem>
                            <PhoneAndroidIcon color="secondary" className='icons-paper' />
                            {mobile.model}
                          </ListItem>
                          : ""}
                        {mobile.price ?
                          <ListItem>
                            <LocalOfferIcon color="secondary" className='icons-paper' />
                            {mobile.price}€
                          </ListItem>
                          : ""}
                      </List>
                      <Button className='details-button' color="secondary" variant="outlined" onClick={() => { setMobileId(mobile.id); navigate('/description'); }}>{texts.ShowDetails[language]}</Button>
                    </div>
                  </div>
                </Paper>
              );
            })}
          </div>
        }
        <div className='div-row-center'>
          <Typography style={theme === "Dark Theme" ? { color: "white", marginTop: "5px" } : { marginTop: "5px" }}>{texts.Page[language] + page}</Typography>
          <Pagination count={Math.ceil(data.length / paginationSize) ? Math.ceil(data.length / paginationSize) : 1} page={page} onChange={handlePaginationChange} color="secondary" />
        </div>
      </div>
    );
  };

  return (
    renderMobileList()
  );
}
