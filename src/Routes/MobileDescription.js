
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { List, CircularProgress, ListItem, MenuItem, Paper, Select, Typography, Button } from "@mui/material";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import StoreIcon from '@mui/icons-material/Store';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import DeveloperBoardIcon from '@mui/icons-material/DeveloperBoard';
import MemoryIcon from '@mui/icons-material/Memory';
import SystemSecurityUpdateIcon from '@mui/icons-material/SystemSecurityUpdate';
import { mobileDataUrl, mobileDataPathGet } from './MobileList.js';
import { setHtmlStorage, statusHtmlStorage, removeHtmlStorage } from '../Utils/LocalStorageCache.js';
import { AspectRatio, Battery3Bar, CameraAlt, EventAvailable, FlipCameraAndroid, Fullscreen, NetworkWifi, Scale, SdCard, ShoppingBasket, SimCard } from '@mui/icons-material';
import { AxiosGet, AxiosPost } from '../Utils/Calls.js';
import textsJSON from "../Texts/texts.json";

const texts = textsJSON.MobileDescription; //Texts for multilanguage

const mobileDataPathPost = '/api/cart/'; //Add to cart endpoint

/*
  Mobile description view
  - Paper that shows:
    - Mobile image
    - Colors and storage selectors
    - Description with the following items (if the backend has):
      - Brand
      - Model
      - Price
      - CPU
      - RAM
      - OS
      - Internal storage
      - Screen size
      - Battery
      - Main camera
      - Secondary camera
      - Dimentions
      - Weight
      - Sim
      - Network speed
      - Date of release
*/
export default function MobileDescription(language, mobileId, addProduct, theme) {
  const [info, setInfo] = useState(statusHtmlStorage('mobile_info', {})); //Mobile description from backend
  const [color, setColor] = useState(statusHtmlStorage('mobile_color', null)); //Mobile color select
  const [storage, setStorage] = useState(statusHtmlStorage('mobile_storage', null)); //Mobile storage select
  const [disabled, setDisabled] = useState(false); //Disable AddToCart button
  const navigate = useNavigate(); //Navigate through routes with react router

  //Get mobile info from backend if it is not in Local Storage
  useEffect(() => {
    async function getInfo() {
      //Check if mobile info is in Local Storage, then get it
      if (statusHtmlStorage('mobile_info')) {
        setInfo(JSON.parse(localStorage.getItem('mobile_info')));
        if (statusHtmlStorage('mobile_color') && statusHtmlStorage('mobile_storage')) {
          setColor(localStorage.getItem('mobile_color'));
          setStorage(localStorage.getItem('mobile_storage'));
        }
        // Get mobile info from backend and store it
      } else {
        if (mobileId) {
          const data = await AxiosGet(`${mobileDataUrl}${mobileDataPathGet}${mobileId}`);
          const dataInfo = data.data;
          setInfo(dataInfo);
          setHtmlStorage('mobile_info', JSON.stringify(dataInfo), 3600);
          if (dataInfo.options && dataInfo.options.colors) {
            setColor(dataInfo.options.colors[0].code);
            setHtmlStorage('mobile_color', dataInfo.options.colors[0].code, 3600);
          }
          if (dataInfo.options && dataInfo.options.storages) {
            setStorage(dataInfo.options.storages[0].code);
            setHtmlStorage('mobile_storage', dataInfo.options.storages[0].code, 3600);
          }
        }
      }
    }
    getInfo();
    setDisabled(false);
  }, [mobileId]);

  //Render select options (color, storages) if the mobile info has these options
  const renderOptions = (options) => {
    let colors = null;
    let storages = null;
    if (options.colors) {
      colors = (
        <div className='div-margin-right'>
          <Typography>{texts.Colors[language]}</Typography>
          <Select
            labelId="colors-select"
            id="colors"
            color="secondary"
            value={color}
            label="Colors"
            defaultValue={options.colors[0].code}
            onChange={(event) => setColor(event.target.value)}
          >
            {options.colors.map(color => { return (<MenuItem key={`color_${color.code}`} value={color.code}>{color.name}</MenuItem>) })}
          </Select>
        </div>
      );
    }

    if (options.storages) {
      storages = (
        <div>
          <Typography>{texts.Storages[language]}</Typography>
          <Select
            labelId="storages-select"
            id="storages"
            color="secondary"
            value={storage}
            label="Storages"
            onChange={(event) => setStorage(event.target.value)}
          >
            {options.storages.map(storage => { return (<MenuItem key={`storage_${storage.code}`} value={storage.code}>{storage.name}</MenuItem>) })}
          </Select>
        </div>
      );
    }

    return (
      <div className='div-row-center'>
        {colors}
        {storages}
      </div>
    );
  }

  //Add to cart mobile, send it to the backend
  const addToCart = async () => {
    const data = {
      'id': info.id,
      'colorCode': color,
      'storageCode': storage
    }
    await AxiosPost(`${mobileDataUrl}${mobileDataPathPost}`, data);
    // setProducts(response.data.count) //Save count products of backend
    addProduct(info);
    //Store products in Local Storage
    if (localStorage.getItem('cart_products')) {
      console.log(localStorage.getItem('cart_products'))
      console.log(JSON.parse(localStorage.getItem('cart_products')))
      console.log(JSON.parse(localStorage.getItem('cart_products')).concat(info))
      setHtmlStorage('cart_products', JSON.stringify(JSON.parse(localStorage.getItem('cart_products')).concat(info)), 3600); //Store products count
    } else {
      setHtmlStorage('cart_products', JSON.stringify([info]), 3600);
    }
    setDisabled(true);
  }
  
  return (
    <div className='div-row-center-paper' style={theme === "Dark Theme" ? { backgroundColor: "#1b1b1b" } : { backgroundColor: "#f9f9f9" }}>
      {Object.keys(info).length === 0 ?
        <CircularProgress style={{ width: "100px", height: "100px" }} className="circular-progress" /> :
        <Paper className="paper-description" elevation={5}>
          <div>
            <img className='img-description' src={info.imgUrl} alt={info.model} />
            {info.options ? renderOptions(info.options) : null}
          </div>
          <div>
            <Typography className='typography-description'>{texts.Description[language]}</Typography>
            <List>
              {info.brand ?
                <ListItem>
                  <StoreIcon color="secondary" className='icons-paper' />
                  {info.brand}
                </ListItem>
                : ""}
              {info.model ?
                <ListItem>
                  <PhoneAndroidIcon color="secondary" className='icons-paper' />
                  {info.model}
                </ListItem>
                : ""}
              {info.price ?
                <ListItem>
                  <LocalOfferIcon color="secondary" className='icons-paper' />
                  {info.price}€
                </ListItem>
                : ""}
              {info.cpu ?
                <ListItem>
                  <DeveloperBoardIcon color="secondary" className='icons-paper' />
                  {info.cpu}
                </ListItem>
                : ""}
              {info.ram ?
                <ListItem>
                  <MemoryIcon color="secondary" className='icons-paper' />
                  {info.ram}
                </ListItem>
                : ""}
              {info.os ?
                <ListItem>
                  <SystemSecurityUpdateIcon color="secondary" className='icons-paper' />
                  {info.os}
                </ListItem>
                : ""}
              {info.externalMemory ?
                <ListItem>
                  <SdCard color="secondary" className='icons-paper' />
                  {info.externalMemory}
                </ListItem>
                : ""}
              {info.displaySize ?
                <ListItem>
                  <AspectRatio color="secondary" className='icons-paper' />
                  {info.displaySize}
                </ListItem>
                : ""}
              {info.battery ?
                <ListItem>
                  <Battery3Bar color="secondary" className='icons-paper' />
                  {info.battery}
                </ListItem>
                : ""}
              {info.primaryCamera ?
                <ListItem>
                  <CameraAlt color="secondary" className='icons-paper' />
                  {info.primaryCamera}
                </ListItem>
                : ""}
              {info.secondaryCamera ?
                <ListItem>
                  <FlipCameraAndroid color="secondary" className='icons-paper' />
                  {info.secondaryCamera}
                </ListItem>
                : ""}
              {info.dimentions ?
                <ListItem>
                  <Fullscreen color="secondary" className='icons-paper' />
                  {info.dimentions}
                </ListItem>
                : ""}
              {info.weight ?
                <ListItem>
                  <Scale color="secondary" className='icons-paper' />
                  {info.weight}
                </ListItem>
                : ""}
              {info.sim ?
                <ListItem>
                  <SimCard color="secondary" className='icons-paper' />
                  {info.sim}
                </ListItem>
                : ""}
              {info.networkSpeed ?
                <ListItem>
                  <NetworkWifi color="secondary" className='icons-paper' />
                  {info.networkSpeed}
                </ListItem>
                : ""}
              {info.status ?
                <ListItem>
                  <EventAvailable color="secondary" className='icons-paper' />
                  {info.status}
                </ListItem>
                : null}
            </List>
            <div className='div-row-center'>
              <Button className='action-buttons' color="secondary" variant="outlined" onClick={() => { setDisabled(false); navigate('/'); removeHtmlStorage('mobile_info'); removeHtmlStorage('mobile_color'); removeHtmlStorage('mobile_storage'); }}>
                <ArrowBackIcon className='icons-paper' fontSize="medium" />
                <Typography>{texts.Back[language]}</Typography>
              </Button>
              <Button disabled={disabled} className='action-buttons' color="secondary" variant="outlined" onClick={addToCart}>
                <ShoppingBasket className='icons-paper' fontSize="medium" />
                <Typography>{texts.AddToCart[language]}</Typography>
              </Button>
            </div>
          </div>
        </Paper>
      }
    </div>
  );
};
