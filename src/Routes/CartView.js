
import { Typography, Paper, Button, Select, MenuItem, IconButton, Divider } from "@mui/material";
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import StoreIcon from '@mui/icons-material/Store';
import { useNavigate } from "react-router-dom";
import textsJSON from "../Texts/texts.json";
import { ArrowBack, Delete, LocalShipping, PhoneAndroid } from "@mui/icons-material";

const texts = textsJSON.CartView; //Texts for multilanguage

/*
  Cart view
  - Button to go to the mobile list
  - List of items of the cart
  - Purchase summary
*/
export function CartView(language, theme, products, setProducts) {
  const navigate = useNavigate(); //Navigate through routes with react router

  //Remove items from the cart
  const handleDelete = (product) => {
    setProducts(products.filter(function (value, index, arr) {
      return value !== product;
    }));
  };

  //Calculate price cart
  const getPrice = () => {
    let price = 0;
    for (let i = 0; i < products.length; i++) {
      if (products[i].price) {
        price += parseInt(products[i].price);
      }
    }
    return price;
  }

  return (
    <div style={theme === "Dark Theme" ? { backgroundColor: "#1b1b1b" } : { backgroundColor: "#f9f9f9" }}>
      <Button onClick={() => { navigate('/'); }} className="cart-button-back" color="secondary">
        <ArrowBack className='icons-paper' fontSize="medium" />
        <Typography>{texts.Back[language]}</Typography>
      </Button>
      <Typography style={theme === "Dark Theme" ? { color: "white" } : {}} className='typography-cart-title'>{texts.Title[language]}</Typography>
      <div className="div-row-column">
        <div className="cart-list">
          {products.map((product, index) => {
            return (
              <Paper className="cart-mobile-papers" elevation={3} key={product.id + index}>
                <div className="div-row">
                  <img src={product.imgUrl} alt={product.model} className="img-cart" />
                  <div className="div-column">
                    <div className="div-row-100">
                      <div>
                        {product.brand ?
                          <Typography className="cart-modelbrand">
                            <StoreIcon color="secondary" className='icons-paper-cart' />
                            {product.brand}
                          </Typography>
                          : ""}
                        {product.model ?
                          <Typography className="cart-modelbrand">
                            <PhoneAndroidIcon color="secondary" className='icons-paper-cart' />
                            {product.model}
                          </Typography>
                          : ""}
                      </div>
                      <div className="div-flex" />
                      <div>
                        {product.price ?
                          <Typography className="cart-price">
                            <LocalOfferIcon color="secondary" className='icons-paper-cart' />
                            {product.price}€
                          </Typography>
                          : ""}
                      </div>
                      {/* <Button className='details-button' color="secondary" variant="outlined" onClick={() => { setMobileId(mobile.id); navigate('/description'); }}>{texts.ShowDetails[language]}</Button> */}
                    </div>
                    <div className="div-column-center-cart">
                      <div className="div-row-center-cart">
                        <Typography className="items-cart">Items:</Typography>
                        <Select disabled defaultValue={1}>
                          <MenuItem value={1}>1</MenuItem>
                          <MenuItem value={2}>2</MenuItem>
                          <MenuItem value={3}>3</MenuItem>
                          <MenuItem value={4}>4</MenuItem>
                          <MenuItem value={5}>5</MenuItem>
                          <MenuItem value={6}>6</MenuItem>
                        </Select>
                      </div>
                      <div className="div-row-center-cart">
                        <Typography className="items-cart">{texts.Remove[language]}</Typography>
                        <IconButton onClick={() => { handleDelete(product) }} className="delete-icon">
                          <Delete color="error" />
                        </IconButton>
                      </div>
                    </div>
                  </div>
                </div>
              </Paper>
            );
          })}
        </div>
        <div className="div-row-center-paper-cart">
          <Paper className="paper-summary">
            <Typography className="cart-summary">{texts.Summary[language]}</Typography>
            <div className="div-row-normal">
              <PhoneAndroid className="icons-paper" color="secondary" />
              <Typography><b className="bold-typography">{products.length}</b>{texts.Articles[language]}</Typography>
            </div>
            <div className="div-row-normal">
              <LocalShipping className="icons-paper" color="secondary" />
              <Typography>{texts.Ship[language]}<b className="bold-typography">{texts.Free[language]}</b></Typography>
            </div>
            <Divider />
            <Typography className="cart-total">{texts.Total[language]}</Typography>
            <Typography className="price-total">{getPrice()}€</Typography>
            <div className="div-center">
              <Button disabled={products.length === 0} variant="contained" className="" onClick={() => { navigate('/'); }}>{texts.Ordering[language]}</Button>
            </div>
          </Paper>
        </div>
      </div>
    </div>
  );
}