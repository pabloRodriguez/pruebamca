import { useEffect, useState } from 'react';
import { Routes, Route } from "react-router-dom";
import { ThemeProvider, createTheme } from '@mui/material/styles';
import './Styles/App.css';
import ApplicationBar from './Components/ApplicationBar.js';
import MobileList from './Routes/MobileList.js';
import MobileDescription from './Routes/MobileDescription.js';
import { statusHtmlStorage } from './Utils/LocalStorageCache.js';
import FooterCustom from './Components/FooterCustom.js';
import { IconButton } from '@mui/material';
import { ArrowUpward } from '@mui/icons-material';
import { CartView } from './Routes/CartView';

//Theme with the main colors of the applicacion (light theme)
const lightTheme = createTheme({
  palette: {
    primary: {
      light: '#4383cc',
      main: '#1565c0',
      dark: '#0e4686'
    },
    secondary: {
      light: '#df487f',
      main: '#d81b60',
      dark: '#971243'
    }
  },
});

//Dark theme of the application (change paper background)
const darkTheme = createTheme({
  palette: {
    mode: 'dark',
    background: {
      paper: "#1b1b1b"
    }
  },
});


/* Main application component
   - ThemeProvider to organise styles with a theme
   - ApplicationBar of the app
   - Routes:
     - '/': Home (MobileList)
     - '/description': MobileDescription
   - Button to go up
   - Footer
*/
function App() {
  const [language, setLanguage] = useState('EN'); //Application language (EN, ES)
  const [mobileId, setMobileId] = useState(''); //Mobile id for description mobile view 
  const [products, setProducts] = useState(statusHtmlStorage('cart_products') ? JSON.parse(localStorage.getItem('cart_products')) : []); //Products of the cart
  const [theme, setTheme] = useState('Light Theme'); //Application theme (Light Theme, Dark Theme)

  // When the user scrolls down 20px from the top of the document, show a button to scroll up
  useEffect(() => {
    let upButton = document.getElementById("upButton");
    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        upButton.style.display = "block";
      } else {
        upButton.style.display = "none";
      }
    }
    window.onscroll = function () { scrollFunction() };
  }, []);

  //Scroll up when clicking upButton
  const goUp = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  const addProduct = (product) => {
    console.log(products)
    setProducts(products.concat(product))
  }
  console.log(products)
  return (
      <ThemeProvider theme={theme === 'Light Theme' ? lightTheme : darkTheme}>
        <ApplicationBar language={language} setLanguage={setLanguage} products={products} theme={theme} setTheme={setTheme} />
        <Routes>
          <Route path='/' element={MobileList({ language, setMobileId, theme })} />
          <Route path={`/description`} element={MobileDescription(language, mobileId, addProduct, theme)} />
          <Route path={`/cart`} element={CartView(language, theme, products, setProducts)} />
        </Routes>
        <IconButton className="up-button" color="secondary" id="upButton" onClick={goUp}>
          <ArrowUpward fontSize='large' />
        </IconButton>
        <FooterCustom language={language} theme={theme} />
      </ThemeProvider>
  );
}

export default App;
