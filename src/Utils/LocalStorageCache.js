//Remove an item from Local Storage with its expiration item
export function removeHtmlStorage(name) {
  localStorage.removeItem(name);
  localStorage.removeItem(name + '_time');
}

//Set an item to Local Storage with its expiration item
export function setHtmlStorage(name, value, expires) {
  if (expires === undefined || expires === null) { expires = 3600; } // 1h
  let date = new Date();
  let schedule = Math.round((date.setSeconds(date.getSeconds() + expires)) / 1000);
  localStorage.setItem(name, value);
  localStorage.setItem(name + '_time', schedule);
}

//Check if an item is in Local Storage. Return the item if the value exists and it is not expired. If not, return defaultValue
export function statusHtmlStorage(name, defaultValue) {
  let date = new Date();
  let current = Math.round(+date / 1000);
  let stored_time = localStorage.getItem(name + '_time');
  if (stored_time === undefined || stored_time === null) { stored_time = 0; }
  if (stored_time < current) {
    removeHtmlStorage(name);
    return defaultValue;
  } else {
    let item = {};
    try {
      item = localStorage.getItem(name);
    } catch (e) { }
    if (typeof (defaultValue) === "object") { item = JSON.parse(item); }
    return item;
  }
}
