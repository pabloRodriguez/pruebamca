import axios from 'axios';

//Get call using Axios, 3 attempts maximum or returns [] if status is not 200
export async function AxiosGet(url) {
  let attempts = 0;
  while (attempts < 2) {
    const response = await axios.get(url);
    if (response.status === 200) {
      return response;
    }
    attempts++;
  }
  return [];
}

//Post call using Axios, 3 attempts maximum or returns [] if status is not 200
export async function AxiosPost(url, data) {
  let attempts = 0;
  while (attempts < 2) {
    const response = await axios.post(url, data);
    if (response.status === 200) {
      return response;
    }
    attempts++;
  }
  return [];
}