import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import { BrowserRouter } from 'react-router-dom';
import App from './App';

test('renders The Mobile House', async () => {
  await render(<BrowserRouter><App /></BrowserRouter>);
  const linkElement = screen.getByText(/The Mobile House/i);
  expect(linkElement).toBeInTheDocument();
});

test('title link to home', async () => {
  await render(<BrowserRouter><App /></BrowserRouter>);
  fireEvent.click(screen.getByText('The Mobile House'));
  expect(global.window.location.pathname).toEqual('/');
});

test('renders Breadcrumbs', async () => {
  await render(<BrowserRouter><App /></BrowserRouter>);
  const linkElement = screen.getByText(/Home/i);
  expect(linkElement).toBeInTheDocument();
});

test('change language', async () => {
  await render(<BrowserRouter><App /></BrowserRouter>);
  fireEvent.click(screen.getByText('EN'));
  await waitFor(() => screen.getByRole('heading'));
  expect(screen.getByRole('heading')).toHaveTextContent('Inicio');
});

test('tooltip Cart', async () => {
  await render(<BrowserRouter><App /></BrowserRouter>);
  userEvent.hover(screen.getByLabelText('shop-button'));
  expect(screen.getByLabelText('Cart')).toBeInTheDocument();
  userEvent.unhover(screen.getByLabelText('shop-button'));
  expect(screen.queryByText('Cart')).not.toBeInTheDocument();
});

test('change to dark theme', async () => {
  await render(<BrowserRouter><App /></BrowserRouter>);
  fireEvent.click(screen.getByRole('switch'));
  expect(screen.queryByRole('heading')).toHaveStyle('background-image: linear-gradient(rgba(255, 255, 255, 0.09), rgba(255, 255, 255, 0.09))');
});

test('renders typographys of top mobile list view', async () => {
  await render(<BrowserRouter><App /></BrowserRouter>);
  let linkElement = screen.getByText(/List view of products:/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Filters:/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Number of products:/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders footer', async () => {
  await render(<BrowserRouter><App /></BrowserRouter>);
  let linkElement = screen.getByText(/Help/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Account/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Company/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Contact/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Support/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Privacy/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Legal advice/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Login/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Register/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Conditions/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Who we are?/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByText(/Work with us/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByTestId(/FacebookIcon/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByTestId(/InstagramIcon/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByTestId(/TwitterIcon/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByTestId(/YouTubeIcon/i);
  expect(linkElement).toBeInTheDocument();
  linkElement = screen.getByTestId(/PinterestIcon/i);
  expect(linkElement).toBeInTheDocument();
});
