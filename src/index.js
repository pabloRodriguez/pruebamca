import React from 'react';
import { createRoot } from 'react-dom/client';
import './Styles/index.css';
import App from './App';
import { BrowserRouter } from 'react-router-dom';

const container = document.getElementById('root');
const root = createRoot(container); //Render with React 18 update sintax
root.render(
  <React.StrictMode>
    <BrowserRouter> {/* BrowserRouter to organise views with routes */}
      <App />
    </BrowserRouter>
  </React.StrictMode >
);

